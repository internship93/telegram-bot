

const arr = [ { chat_id: 495145609 }, { chat_id: 495145610 }, { chat_id: 49514561 }, { chat_id: 495145612 }, { chat_id: 495145613 } ]


export function sort(arr) {
	let flag = true;
	let i = 0;
	let j = arr.length -1 
	while(i < j && flag){
	  flag = false;
	  for(let k = i; k < j; k++) {
		if (arr[k].chat_id > arr[k+1].chat_id ) {
		  [arr[k].chat_id , arr[k+1].chat_id ] = [arr[k+1].chat_id , arr[k].chat_id ]
		  flag = true;
		}
	  } 
		j -= 1;
	   if (flag){
		  flag = false;
		  for (let k = j; k > i; k--){
		   if (arr[k].chat_id  < arr[k - 1].chat_id ){
			 [arr[k].chat_id , arr[k - 1].chat_id ] = [arr[k - 1].chat_id , arr[k].chat_id ];
			 flag = true;
			}
		  }
		}
	   i += 1;
	}
	return arr;
}

export const binarySearch = (arr, el) =>{
	let left = -1;
	let right = arr.length;
	while (right - left > 1){

		const mid = Math.floor((left + right) / 2);
		if (arr[mid]['chat_id'] == el){
			return 1;
		}
		if(arr[mid]['chat_id'] > el){
			right = mid;
		} else {
			left = mid;
		}
	}
	return -1;
}
