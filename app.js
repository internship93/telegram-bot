import { PORT, TOKEN, CONFIG_DB } from './config.js'
import { sort, binarySearch } from './datahadler.js'
import { Telegraf } from 'telegraf'
import { getMainMenu, getMailingBtn } from './keyboards.js'
import { fileURLToPath } from 'url';
import postgresql from 'pg';
import express from 'express'
import path from 'path';
import fs from 'fs';

const appid ="b31f7751cdfac1050d2705a47ff9f3d3"
const app = express()
const bot = new Telegraf(TOKEN)


const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);


// На случай если захочется искать температуру по координатам
let lat = "49.2497"
let lon = "-123.1193"
const urlLatLon= `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${appid}`

// На случай если захочется искать температуру по городу
let city = 'Vancouver'
const urlCity= `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${appid}`

const { Pool } = postgresql;



bot.start(ctx => {
  ctx.reply('Здравствуйте. Нажмите на любую интересующую Вас кнопку', getMainMenu())

  const checkChatId = async (config) =>{

    const pool = new Pool(config)
    await pool.query('SELECT chat_id FROM bot_users', (err, result) => {
      const dbResponse = result.rows;

      if (binarySearch(sort(dbResponse), ctx.chat.id) === -1){
        pool.end();
        console.log('Такой пользователь еще не был');
        insertUser(CONFIG_DB);
        return;
      } else{
        console.log('Такой пользователь существует');
        pool.end();
        return;
      }
    }) 
  }

  const insertUser = async (config) =>{
    const pool = new Pool(config);
    await pool.query(`INSERT INTO bot_users (chat_id, first_name, username, type) VALUES ('${ctx.chat.id}','${ctx.chat.first_name}','${ctx.chat.username}','${ctx.chat.type}')`, (e, res) => {
      if (e){
        throw new Error(e)
      } else{
        console.log("Пользователь добавлен");
      }
    });
  pool.end();
  }
  checkChatId(CONFIG_DB);
});

bot.hears('Погода в Канаде', ctx => {
  fetch(urlCity)
  	.then(function(resp) {return resp.json() })
  	.then(function (data) {
      let temp = Math.round(data.main.temp - 273.15);
      let lastNumberTemp = temp.toString().split('');
      if (temp === 1 || lastNumberTemp[lastNumberTemp.length - 1] === '1'){
        temp = temp + " градус."
      } else if(temp === 2 || temp === 3 || temp === 4) {
        temp = temp + " градуса."
      } else {
        temp += " градусов."
      }
      if (temp < 10){
        ctx.reply(`Температура в Ванкувере ${temp} \nКак же холодно!`)
      } else if(temp > 10 && temp < 15){
        ctx.reply(`Температура в Ванкувере ${temp} \nПрохладненько...`)
      } else {
        ctx.reply(`Температура в Ванкувере ${temp} \nКак же я им завидую. Не то, чтобы бот мог мерзнуть, но все же.`)
      }  
    }) 
})

bot.hears('Хочу почитать!', async (ctx) => {
  ctx.telegram.sendChatAction(ctx.chat.id, 'upload_photo')
   await ctx.replyWithPhoto(
    { url: 'https://pythonist.ru/wp-content/uploads/2020/03/photo_2021-02-03_10-47-04-350x2000-1.jpg' },
    { caption: 'Идеальный карманный справочник для быстрого ознакомления с особенностями работы разработчиков на Python. Вы найдете море краткой информации о типах и операторах в Python, именах специальных методов, встроенных функциях, исключениях и других часто используемых стандартных модулях.' })
  const archivePath = path.join(__dirname, 'source', "karmaniy_spravochnik_po_piton.zip")
  await ctx.replyWithDocument({
    source: fs.createReadStream(archivePath),
    filename: "karmaniy_spravochnik_po_piton.zip"

  })
});

bot.hears('Сделать рассылку', async ctx => {
  await ctx.reply('Вы выбрали рассылку всем пользователям. Вы уверены что хотите это сделать?',getMailingBtn())
})
bot.hears('Отмена', ctx =>{
  ctx.reply('Возвращаемся к главному меню!', getMainMenu())

});

bot.hears('Уверен', ctx =>{
  ctx.reply("Введите текст сообщения");
  bot.on('text',async ctx => {
    const pool = new Pool(CONFIG_DB)
    await pool.query('SELECT chat_id FROM bot_users', (err, result) => {
      const dbResponse = result.rows;
      for(let i = 0; i < dbResponse.length;i++){
        ctx.telegram.sendMessage(dbResponse[i].chat_id, ctx.update.message.text)
      }
      pool.end();
    })
  })
})

bot.launch()
app.listen(PORT, () => console.log(`My server is running on port ${PORT}`))