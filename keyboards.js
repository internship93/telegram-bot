import { Markup } from 'telegraf'

export function getMainMenu() {
    return Markup.keyboard([
        ["Погода в Канаде", 'Хочу почитать!'],
        ['Сделать рассылку']
    ]).resize()
}
export function getMailingBtn() {
    return Markup.keyboard([
        ["Отмена", 'Уверен'],
    ]).resize()
}

